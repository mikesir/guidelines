# DevCom Code of Conduct


## Introduction

This Code of Conduct builds upon the [Virginia Tech Principles of Community](https://www.inclusive.vt.edu/Initiatives/vtpoc0.html) and applies to all events, locations, and development efforts sponsored by Virginia Tech DevCom. We expect all individuals who participate in these events to honor this code. 

This code is _not_ exhaustive or complete. We expect it to be followed in spirit as much as in the letter, so that it can enrich all of us as we strive to better our IT efforts and the university as a whole.


## Guidelines

As participants in DevCom, we strive to:

1. **Be open to all.** If you write code at all, we welcome you. It does not matter if you are a professional developer, sysadmin, intern or anywhere between, you are welcome to join meetings, present ideas, and contribute to projects. We welcome all backgrounds and reject any form of prejudice or discrimination.
1. **Operate in the open.** We strive to be as transparent as possible. We favor public communication channels (discussion threads on GitLab, the #devcom and #devcom-* Slack channels, etc.) as opposed to private messaging. We post recordings for recorded events and minutes for non-recorded events. If asked a question, we do our best to respond or help point to those that can respond.
1. **Have strong opinions, but hold them loosely.** We work in an environment that fosters passionate creativity, yet is always changing. Be willing to share what works and how you reached your conclusions. While conversations may become heated, be willing to learn from others and, if shown a better path, willing to adopt new ideas and practices.
1. **Be thoughtful in communications.** Online text is inherently difficult to convey emotions and intents. Be _extra_ kind to others. Do not insult or put others down. Any form of harassment is not acceptable.
1. **Give the benefit of the doubt.** Recognizing that others have accepted this Code of Conduct and are attempting to be thoughtful, we strive to read things as positively as possible. Others may have bad days or other extenuating circumstances that affect their communications. We should keep things professional and be thoughtful in response.


## Reporting Guidelines

If you feel or see any issues related to someone's conduct that may be in disharmony with this Code of Conduct, _do not_ hesitate to reach out. Assume good faith; it is more likely that participants are unaware of their behavior than that they intentionally try to degrade the quality of the discussion. Should there be difficulties in dealing with the situation, you may report your compliance issues in confidence to either:

- TODO #1
- TODO #2

